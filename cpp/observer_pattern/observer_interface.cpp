#include <iostream>

#include "include\subject_interface.hpp"
#include "include\observer_interface.hpp"

ObserverInterface::ObserverInterface(SubjectInterface *subject, int observer_id) {
	this->id = observer_id;
	this->subject = subject;
}

void ObserverInterface::register_observer() {
	this->subject->add_observer(this);
}

void ObserverInterface::unregister_observer() {
	this->subject->remove_observer(this);
}