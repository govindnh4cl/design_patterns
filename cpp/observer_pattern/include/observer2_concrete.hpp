#pragma once
#include <iostream>

#include "observer_interface.hpp"

class Observer2Concrete : public ObserverInterface {
private:
	int temperature;
	int pressure;

public:
	Observer2Concrete(SubjectInterface *subject, int observer_id) :
		ObserverInterface(subject, observer_id) {
		this->temperature = 0;
		this->pressure = 0;
	}

	void update(float temperature, float pressure) {
		this->temperature = static_cast<int>(temperature);
		this->pressure = static_cast<int>(pressure);
		std::cout << "Observer: Observer " << this->get_id() << " updated. T = " << this->temperature
			<< " P = " << this->pressure << std::endl;
	}

	~Observer2Concrete() {}
};