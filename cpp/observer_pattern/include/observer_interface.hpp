#pragma once
#include <iostream>

class SubjectInterface;  //Forward declaration

class ObserverInterface {
private:
	int id = -1;
	SubjectInterface *subject = NULL;

public:
	ObserverInterface(SubjectInterface *subject, int observer_id);
	int get_id() { return this->id; }
	int set_id(int observer_id) { this->id = observer_id; }
	virtual void update(float temprature, float presssure) = 0;
	void register_observer();
	void unregister_observer();

	~ObserverInterface() {}
};