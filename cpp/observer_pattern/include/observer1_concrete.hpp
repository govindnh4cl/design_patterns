#pragma once
#include <iostream>

#include "observer_interface.hpp"

class Observer1Concrete : public ObserverInterface {
private:
	float temperature;
	float pressure;
public:
	Observer1Concrete(SubjectInterface *subject, int observer_id) :
		ObserverInterface(subject, observer_id) {
		this->temperature = 0;
		this->pressure = 0;
	}

	void update(float temperature, float pressure) {
		this->temperature = temperature;
		this->pressure = pressure;
		std::cout << "Observer: Observer " << this->get_id() << " updated. T = " << this->temperature 
			<< " P = " << this->pressure << std::endl;
	}

	~Observer1Concrete() {}
};