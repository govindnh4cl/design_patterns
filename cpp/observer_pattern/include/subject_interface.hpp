#pragma once
#include <iostream>

#include "observer_interface.hpp"

class SubjectInterface {
public:
	SubjectInterface() {
		return;
	}

	virtual void add_observer(ObserverInterface* observer) = 0;
	virtual void remove_observer(ObserverInterface* observer) = 0;

	~SubjectInterface() {
		return;
	}
};