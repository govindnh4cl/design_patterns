#pragma once
#include <vector>
#include <iostream>

#include "subject_interface.hpp"
#include "observer_interface.hpp"

class SubjectConcrete : public SubjectInterface {
private:
	std::vector<ObserverInterface*> observer_list;

public:
	float temperature;
	float pressure;

	SubjectConcrete() : SubjectInterface() {
		this->temperature = 0;
		this->pressure = 0;
	}

	void add_observer(ObserverInterface* observer) override {
		auto temp_obs = std::find(observer_list.begin(), observer_list.end(), observer);

		if (temp_obs == observer_list.end()) {
			this->observer_list.push_back(observer);
			std::cout << "Subject: Added observer: " << observer->get_id() << std::endl;
		}
		else {
			std::cout << "Subject: Observer " << observer->get_id() << " already in list. Not adding.\n";
		}
	}

	void remove_observer(ObserverInterface* observer) override {
		// Search for observer in list
		auto temp_obs = std::find(observer_list.begin(), observer_list.end(), observer);
		if (temp_obs == observer_list.end()) {
			// Return false if observer not found.
			std::cout << "Subject: Didn\'t remove Observer: " << observer->get_id() <<
				"from observer list as this observer does not exist in the list." << std::endl;
		}

		this->observer_list.erase(temp_obs);  // Remove observer from list
		std::cout << "Subject: Removed Observer: " << observer->get_id() << " from observer list." << std::endl;
	}

	void set_pressure(float pressure) { 
		this->pressure = pressure; 
		for (auto const& it:this->observer_list) {
			it->update(this->temperature, this->pressure);
		}
	}

	void set_temperature(float temperature) { 
		this->temperature = temperature; 
		for (auto const& it : this->observer_list) {
			it->update(this->temperature, this->pressure);
		}
	}

	~SubjectConcrete() {}
};