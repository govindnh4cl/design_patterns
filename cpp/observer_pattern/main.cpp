#include <iostream>

#include "include/subject_concrete.hpp"
#include "include/observer1_concrete.hpp"
#include "include/observer2_concrete.hpp"

int main() {
	auto subject = SubjectConcrete();
	auto observer_1 = Observer1Concrete(&subject, 1);
	auto observer_2 = Observer2Concrete(&subject, 2);

	//This would not have any effect on observers since they aren't registered yet
	subject.set_pressure(1.5);
	subject.set_temperature(100.5);

	// Register observers to subject
	observer_1.register_observer();
	observer_2.register_observer();

	// This should update both observers
	subject.set_pressure(2.5);
	subject.set_temperature(200.5);

	// Remove one of the observer;
	observer_2.unregister_observer();

	// This should update only one observer
	subject.set_pressure(3.5);
	subject.set_temperature(300.5);

	// Register again
	observer_1.register_observer();  // This should give 'already registered' message
	observer_2.register_observer();

	// This should update both observers
	subject.set_pressure(4.5);
	subject.set_temperature(400.5);

	//# Remove both of the observer
	observer_1.unregister_observer();
	observer_2.unregister_observer();
			
	// This should not update any observers
	subject.set_pressure(5.5);
	subject.set_temperature(500.5);
}

