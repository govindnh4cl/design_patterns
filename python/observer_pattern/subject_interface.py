import sys
import os
from abc import ABC, abstractmethod

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from common import logger

log = logger.get_logger()

class SubjectInterface(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def add_ovserver(self, observer):
        raise NotImplementedError

    @abstractmethod
    def remove_ovserver(self, observer):
        raise NotImplementedError


