import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from common import logger
from observer_pattern.subject_concrete import SubjectConcrete
from observer_pattern.observer1_concrete import Observer1Concrete
from observer_pattern.observer2_concrete import Observer2Concrete

log = logger.setup_logger(None)


if __name__ == '__main__':
    subject = SubjectConcrete()
    observer_0 = Observer1Concrete(subject, observer_id=0)
    observer_1 = Observer1Concrete(subject, observer_id=1)

    # This would not have any effect on observers since they aren't registered yet
    subject.set_pressure(1.)
    subject.set_temperature(100.0)

    # Register observers to subject
    observer_0.register_ovserver()
    observer_1.register_ovserver()

    # This should update both observers
    subject.set_pressure(2.)
    subject.set_temperature(200.)

    # Remove one of the observer
    observer_1.unregister_ovserver()

    # This should update only one observer
    subject.set_pressure(3.)
    subject.set_temperature(300.)

    # Register again
    observer_0.register_ovserver()  # This should give 'already registered' message
    observer_1.register_ovserver()

    # This should update both observers
    subject.set_pressure(4.)
    subject.set_temperature(400.)

    # Remove both of the observer
    observer_0.unregister_ovserver()
    observer_1.unregister_ovserver()

    # This should not update any observers
    subject.set_pressure(5.)
    subject.set_temperature(500.)