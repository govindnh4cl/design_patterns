import sys
import os
from abc import ABC, abstractmethod

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from common import logger

log = logger.get_logger()

class ObserverInterface(ABC):
    def __init__(self, subject, observer_id):
        self.subject = subject
        self.observer_id = observer_id
        self.temperature = None
        self.pressure = None

    def register_ovserver(self):
        log.info('Observer: Observer {:d} would try to attach to Subject'.format(self.observer_id))
        self.subject.add_ovserver(self)

    def unregister_ovserver(self):
        log.info('Observer: Observer {:d} would try to detach from Subject'.format(self.observer_id))
        self.subject.remove_ovserver(self)

    @abstractmethod
    def update(self, temperature, pressure):
        """
        Every observer may handle updates differently
        :param temperature: new temperature value
        :param pressure:
        :return:
        """
        raise NotImplementedError
