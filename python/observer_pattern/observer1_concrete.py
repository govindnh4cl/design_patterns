import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from common import logger
from observer_pattern.observer_interface import ObserverInterface

log = logger.get_logger()

class Observer1Concrete(ObserverInterface):
    def __init__(self, subject, observer_id):
        ObserverInterface.__init__(self, subject, observer_id)

    def update(self, temperature, pressure):
        self.temperature = temperature
        self.pressure = pressure
        log.info('Observer: Observer {:d} has received updated params'.format(self.observer_id))
