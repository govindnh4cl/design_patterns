import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from common import logger
from observer_pattern.subject_interface import SubjectInterface

log = logger.get_logger()

class SubjectConcrete(SubjectInterface):
    def __init__(self):
        SubjectInterface.__init__(self)
        self.observer_list = list()  # Empty list
        self.temperature = 0.  # Quantity to be observed
        self.pressure = 0.  # Quantity to be observed

    def add_ovserver(self, observer):
        if observer in self.observer_list:
            log.info('Subject: Observer {:d} already in list. Not adding.'.format(observer.observer_id))
        else:
            self.observer_list.append(observer)
            log.info('Subject: Added Observer: {:d} to observer list'.format(observer.observer_id))

    def remove_ovserver(self, observer):
        if observer in self.observer_list:
            self.observer_list.remove(observer)
            log.info('Subject: Removed Observer: {:d} from observer list'.format(observer.observer_id))
        else:
            log.info('Subject: Didn\'t remove Observer: {:d} from observer list '
                     'as this observer does not exist in the list'.format(observer.observer_id))

    def set_temperature(self, new_value):
        log.info('Subject: Subject has new temperature: {:f}'.format(new_value))
        self.temperature = new_value
        for observer in self.observer_list:
            observer.update(self.temperature, self.pressure)

    def set_pressure(self, new_value):
        log.info('Subject: Subject has new pressure: {:f}'.format(new_value))
        self.pressure = new_value
        for observer in self.observer_list:
            observer.update(self.temperature, self.pressure)








